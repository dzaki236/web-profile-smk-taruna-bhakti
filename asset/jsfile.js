       
        $(document).ready(function () {
            $(".owl-carousel").owlCarousel({
                items: 4,
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 5
                    }
                },
                loop: true,
                autoplay: true,
                nav: true,
                navText: [
                    '<div class="row"><div class="col-md-6"><i class="fa fa-angle-left" aria-hidden="true"></i></div>',
                    '<div class="col-md-6"><i class="fa fa-angle-right" aria-hidden="true"></i></div> </div>'
                ]
            });
        });

        $(function TypedStart() {
            $("#caption").typed({
                strings: ["Welcome to Smk Taruna Bhakti Depok", "SMK BISA!",
                    "Our quality ask be different"
                ],
                typeSpeed: fast
            });
        });
        $(document).ready(function () {
            $(window).scroll(function () {
                if ($(document).scrollTop() > 40) {

                    $("nav").removeClass("bg-transparent text-white navbar-dark").addClass(
                        "change shadow navbar-light text-dark");
                    $(".navbar-toggler").addClass("border-white");
                } else {
                    $(".navbar-toggler").removeClass("border-white");
                    $("nav").removeClass("change navbar-light shadow text-dark").addClass(
                        "bg-transparent text-white navbar-dark ").css("transition", "0.3s");
                }
            });
        });